import { User } from "./user.model";

export interface AuthData {
    login: string
    password: string
}

export interface AuthResult {
    userData: User
    token: string
}