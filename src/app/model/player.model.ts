export interface Player {
    id: number
    name: string
    nationality: string
    price: string
}