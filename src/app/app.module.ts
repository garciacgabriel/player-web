import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './ui/login/login.component';
import { HomeComponent } from "./ui/home/home.component";

import { AppHeaderComponent } from "./ui/app-header/app-header.component";

import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppMaterialModule } from "./app-material.module";
import { AuthInterceptor } from './service/auth.interceptor';
import { MyTeamsAdminComponent } from './ui/admin/my-teams/my-teams.component';
import { MyPlayersAdminComponent } from './ui/admin/my-players/my-players-admin.component';
import { HomeClientComponent } from './ui/client/home/home.component';
import { ListPlayersComponent } from './ui/common/list-players/list-players.component';
import { ListTeamsComponent } from './ui/common/list-teams/list-teams.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    AppHeaderComponent,
    MyTeamsAdminComponent,
    MyPlayersAdminComponent,
    HomeClientComponent,
    ListPlayersComponent,
    ListTeamsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule, 
    ReactiveFormsModule, 
    BrowserAnimationsModule,
    AppMaterialModule
  ],
  providers: [
      { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
