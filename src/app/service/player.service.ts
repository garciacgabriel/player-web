import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Player } from "../model/player.model";
import { WS_MY_PLAYER } from "./endpoints";

@Injectable(
    { providedIn: 'root' }
)
export class PlayerService {

    constructor(private http: HttpClient) {

    }

    findMyPlayers() : Observable<Player[]> {
        return this.http.get<Player[]>(WS_MY_PLAYER)
    }

}