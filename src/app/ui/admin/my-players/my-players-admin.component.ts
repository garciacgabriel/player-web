import { Component, OnInit } from '@angular/core';
import { Player } from 'src/app/model/player.model';
import { PlayerService } from 'src/app/service/player.service';

@Component({
  selector: 'app-my-players-admin',
  templateUrl: './my-players-admin.component.html',
  styleUrls: ['./my-players-admin.component.css']
})
export class MyPlayersAdminComponent implements OnInit {

  players = new Array<Player>();

  constructor(private playerService: PlayerService) { }

  ngOnInit(): void {
      this.playerService.findMyPlayers().subscribe(result => {
      this.players = result;  
  });
  }

}
