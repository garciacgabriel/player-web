import { Component, OnInit } from '@angular/core';
import { Team } from 'src/app/model/team.model';
import { TeamService } from 'src/app/service/team.service';

@Component({
  selector: 'app-my-teams-admin',
  templateUrl: './my-teams.component.html',
  styleUrls: ['./my-teams.component.css']
})
export class MyTeamsAdminComponent implements OnInit {

  teams = new Array<Team>();

  constructor(private teamService: TeamService) { }

  ngOnInit(): void {
      this.teamService.findAll().subscribe(result => {
      this.teams = result;  
  });
  }

}
