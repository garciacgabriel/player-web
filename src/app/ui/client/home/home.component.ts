import { Component, OnInit } from '@angular/core';
import { Player } from 'src/app/model/player.model';
import { PlayerService } from 'src/app/service/player.service';

@Component({
  selector: 'app-home-client',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeClientComponent implements OnInit {

  players = new Array<Player>();

  constructor(private playerService: PlayerService) { }

  ngOnInit(): void {
      this.playerService.findMyPlayers().subscribe(result => {
      this.players = result;  
  });
  }

}
