import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/service/session.service';

@Component({
    selector: 'app-header-bar',
    templateUrl: './app-header.component.html',
    styleUrls: ['./app-header.component.css']
})
export class AppHeaderComponent {

    constructor(private sessionService: SessionService,
                private router: Router) { }


    logout() {
        this.sessionService.logout()
        this.router.navigateByUrl('/login')
    }

    get isAdmin() {
        return this.sessionService.isAdmin
    }

}
